# yuanhua-rpc-with-netty

#### 介绍
基于netty实现的简易RPC框架！了解netty基本用法以及rpc基本原理！

#### 软件架构
spring + netty
![img.png](img.png)

原理：rpc
netty 开启端口监听；将本机服务注册到服务注册中心；服务注册中心使用一个concurrentHashMap扮演；key为服务类全类名，value为服务类；
netty消费者使用netty将数据发送到管道中，handler自动获取信息进行处理；通过消息协议获取调用的目标方法将调用返回结果以同样的方式放到管道中；
消费者在socket中获取数据；


- 生产者：将服务注册到注册中心，并通过netty对外提供访问
- 消费者：将调用信息序列化后通过netty建立连接发送消息；
- 

#### Netty简介
> netty 是一个优秀的网络通信框架，解决了网络通信中遇到的各种难题，只需要客户端与服务端写一段通用的代码就可以轻松的实现C S通信
> 核心实现原理：响应式NIO，


#### 安装教程

1.  git clone
2.  运行 RpcRegistry类
3.  运行 RpcConsumer类

#### 使用说明
1.  git clone
2.  运行 RpcRegistry类
3.  运行 RpcConsumer类



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


