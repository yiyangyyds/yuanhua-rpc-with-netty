package com.yuanhua.consumer.consumer;

import com.yuanhua.consumer.api.IRpcCalc;
import com.yuanhua.consumer.api.IRpcHello;
import com.yuanhua.consumer.consumer.proxy.RpcProxy;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 9:11
 */
public class RpcConsumer {

    public static void main(String [] args) {
        // 本机之间的正常调用
        // IRpcHello iRpcHello = new RpcHelloProvider ();
        //iRpcHello.hello ("Tom");

        // 肯定是用动态代理来实现的
        // 传给它接口，返回一个接口的实例，伪代理
        IRpcHello rpcHello = RpcProxy.create (IRpcHello.class);
        System.out.println (rpcHello.hello ("ZouChangLin"));

        int a = 10;
        int b = 5;
        IRpcCalc iRpcCalc = RpcProxy.create (IRpcCalc.class);

        System.out.println (String.format ("% d + % d = % d", a, b, iRpcCalc.add (a, b)));
        System.out.println (String.format ("% d - % d = % d ", a, b, iRpcCalc.sub (a, b)));
        System.out.println (String.format ("% d * % d = % d", a, b, iRpcCalc.mul (a, b)));
        System.out.println (String.format ("% d / % d = % d", a, b, iRpcCalc.div (a, b)));
    }
}
