package com.yuanhua.consumer.consumer.proxy;

import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 9:12
 */
public class RpcProxyHandler extends ChannelInboundHandlerAdapter {

    private Object result;

    public Object getResponse() {
        return result;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        result = msg;
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println ("client exception is general");
    }
}
