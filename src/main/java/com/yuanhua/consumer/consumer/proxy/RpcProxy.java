package com.yuanhua.consumer.consumer.proxy;

import com.yuanhua.rpc.core.InvokerMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 9:11
 */
public class RpcProxy {

    public static <T> T create(Class<?> clazz) {
        //clazz 传进来本身就是 interface
        MethodProxy proxy = new MethodProxy(clazz);
        T result = (T) Proxy.newProxyInstance (clazz.getClassLoader (), new Class[]{clazz} , proxy);
        return result;
    }

    private static class MethodProxy implements InvocationHandler {
        private Class<?> clazz;

        public MethodProxy(Class<?> clazz) {
            this.clazz = clazz;
        }
        public Object invoke(Object proxy, Method method, Object [] args) throws Throwable {
            // 如果传进来是一个已实现的具体类
            if (Object.class.equals (method.getDeclaringClass ())) {
                try {
                    return method.invoke (this, args);
                } catch (Throwable t) {
                    t.printStackTrace ();
                }
                // 如果传进来的是一个接口（核心)
            } else {
                return rpcInvoke (method, args);
            }
            return null;
        }

        // 实现接口的核心方法
        public Object rpcInvoke(Method method, Object [] args) {
            // 传输协议封装
            InvokerMessage invokerMessage = new InvokerMessage();
            String name = this.clazz.getName();
            String re = name.replace("consumer", "rpc");
            invokerMessage.setClassName (re);
            invokerMessage.setMethodName (method.getName ());
            invokerMessage.setValues (args);
            invokerMessage.setParams (method.getParameterTypes ());

            final RpcProxyHandler consumerHandler = new RpcProxyHandler();
            EventLoopGroup group = new NioEventLoopGroup();

            try {
                Bootstrap bootstrap = new Bootstrap();
                bootstrap.group (group)
                        .channel (NioSocketChannel.class)
                        .option (ChannelOption.TCP_NODELAY, true)
                        .handler (new ChannelInitializer<SocketChannel>() {

                            @Override
                            public void initChannel(SocketChannel ch) throws Exception {
                                ChannelPipeline pipeline = ch.pipeline ();
                                pipeline.addLast ("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                                // 自定义协议编码器
                                pipeline.addLast ("frameEncoder", new LengthFieldPrepender(4));
                                // 对象参数类型编码器
                                pipeline.addLast ("encoder", new ObjectEncoder());
                                // 对象参数类型解码器
                                pipeline.addLast ("decoder", new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled (null)));
                                pipeline.addLast ("handler", consumerHandler);
                            }
                        });
                ChannelFuture future = bootstrap.connect ("127.0.0.1", 8080).sync ();
                future.channel ().writeAndFlush (invokerMessage).sync ();
                future.channel ().closeFuture ().sync ();
            } catch (Exception e) {
                e.printStackTrace ();
            } finally {
                group.shutdownGracefully ();
            }
            return consumerHandler.getResponse ();
        }
    }
}
