package com.yuanhua.rpc.core;

import lombok.Data;

import java.io.Serializable;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 8:54
 */
@Data
public class InvokerMessage implements Serializable {
    // 服务名称
    private String className;
    // 调用哪个方法
    private String methodName;
    // 参数列表
    private Class<?>[] params;
    // 参数值
    private Object [] values;
}
