package com.yuanhua.rpc.provider;

import com.yuanhua.rpc.api.IRpcCalc;
import com.yuanhua.rpc.api.IRpcHello;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 8:55
 */
public class RpcHelloProvider implements IRpcHello {
    @Override
    public String hello(String name) {
        return "Hello, " + name + "!";
    }
}
