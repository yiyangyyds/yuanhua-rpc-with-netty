package com.yuanhua.rpc.provider;

import com.yuanhua.rpc.api.IRpcCalc;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 8:55
 */
public class RpcCalcProvider implements IRpcCalc {
    @Override
    public int add(int a, int b) {
        return a +b;
    }

    @Override
    public int sub(int a, int b) {
        return a - b;
    }

    @Override
    public int mul(int a, int b) {
        return a * b;
    }

    @Override
    public int div(int a, int b) {
        return a / b;
    }
}
