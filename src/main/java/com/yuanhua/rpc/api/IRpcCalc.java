package com.yuanhua.rpc.api;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 8:54
 */
public interface IRpcCalc {

    // 加
    int add(int a, int b);

    // 减
    int sub(int a, int b);

    // 乘
    int mul(int a, int b);

    // 除
    int div(int a, int b);
}
