package com.yuanhua.rpc.api;

/**
 * @author songguoxiang
 * @version 1.0
 * @description: TODO
 * @date 2022/6/13 8:54
 */
public interface IRpcHello {

    String hello(String name);
}
